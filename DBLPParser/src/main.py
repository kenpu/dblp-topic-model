from dblp_parser import *
import argparse
import os
import sys

parsers = {
        'author': parse_author,
        'article': parse_article,
        'inproceedings': parse_inproceedings,
        'proceedings': parse_proceedings,
        'book': parse_book,
        'publications': parse_publications
        }

def run(dblp_path, output_path, entity):
    if not entity in parsers:
        print("Unknown entity: %s" % args.entity, file=sys.stderr)
        sys.exit(1)

    parser = parsers[entity]
    save_path = os.path.join(output_path, "%s.csv" % entity)

    parser(dblp_path, save_path, save_to_csv=True, include_key=True)

def main():
    parser = argparse.ArgumentParser(description='dblp2csv')
    parser.add_argument('--dblp-dir', 
            dest='dblp_path', 
            required=True,
            help='directory containing dblp.xml and dblp.dtd')
    parser.add_argument('--output-dir',
            dest='output_path',
            help='path to directory that saves the outputs')
    parser.add_argument('--entity',
            dest='entity',
            default='article',
            help='author,article,inproceedings,proceedings,book,publications')
    args = parser.parse_args()

    if not args.dblp_path:
        parser.print_usage()
        sys.exit(1)

    if not args.output_path:
        args.output_path = args.dblp_path

    dblp_path = os.path.join(args.dblp_path, "dblp.xml")

    for entity in args.entity.split(","):
        run(dblp_path, args.output_path, entity)

if __name__ == '__main__':
    main()

