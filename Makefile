all:
	echo "Check targets"

data/dblp.xml: data/dblp.xml.gz
	mkdir -p ./data
	cd ./data && wget https://dblp.uni-trier.de/xml/dblp.xml.gz
	cd ./data && gunzip dblp.xml.gz

data/dblp.dtd:
	mkdir -p ./data 
	cd ./data && wget https://dblp.uni-trier.de/xml/dblp.dtd

tsv:
	./src/make_tsv.sh ./data

repl:
	PYTHONPATH=./src ipython3

count:
	PYTHONPATH=./src \
	cat data/tsv/inproceedings.csv \
	| pv -p -e -l -s `wc -l data/tsv/inproceedings.csv` \
	| python3 src/dblp.py
dct:
	PYTHONPATH=./src \
	cat data/tsv/inproceedings.csv \
	| pv -p -e -l -s `wc -l data/tsv/inproceedings.csv` \
	| python3 src/build_dictionary.py data/dct/inproceedings.dct

model:
	PYTHONPATH=./src \
	cat data/tsv/inproceedings.csv \
	| pv -p -e -l -s `wc -l data/tsv/inproceedings.csv` \
	| python3 src/build_model.py \
		--dct data/dct/inproceedings.dct \
		--topics 50 \
		--output data/lda/inproceedings.lda

check:
	mkdir -p data/output
	PYTHONPATH=./src \
	python3 src/check_model.py \
		--dct data/dct/inproceedings.dct \
		--model data/lda/inproceedings.lda > data/output/inproceedings.txt

word2vec:
	mkdir -p data/wordvecs
	PYTHONPATH=./src \
	python3 src/convert_glove_to_word2vec.py \
		--glove data/glove/glove.6B.100d.txt

clean:
	rm -rf data/lda data/dct data/output
