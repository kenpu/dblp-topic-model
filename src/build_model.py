import os
from time import time
from gensim.corpora import Dictionary
from gensim.models.ldamulticore import LdaMulticore
import dblp
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument('--dct', required=True)
parser.add_argument('--output', required=True)
parser.add_argument('--topics', default=10, type=int)

args = parser.parse_args()

try:
    os.makedirs(os.path.dirname(args.output))
except:
    pass

s = time.time()

dct = Dictionary.load(args.dct)

print("Dct loaded in %.2f seconds" % (time.time() - s))
s = time.time()

docs = dblp.iter_document(dblp.iter_text())
docs = map(dblp.NGramProcessor(2, include_doc=True), docs)

corpus = list(dblp.iter_corpus(docs, dct))

model = LdaMulticore(corpus=corpus, num_topics=args.topics, id2word=dct, workers=10)
model.save(args.output)

print("Model built in %.2f seconds" % (time.time() - s))
