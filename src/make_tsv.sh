#!/bin/bash

export PYTHONPATH=$PWD/DBLPParser/src

data=$1

if [[ -z "$data" ]]
then
    echo "Usage [datadir]"
    exit
fi

mkdir -p $data/tsv

for e in book proceedings article author inproceedings
do
    echo -- parsing $e --
    python3 ./DBLPParser/src/main.py --dblp-dir $data --output-dir $data/tsv --entity $e &
done

wait
