import sys
import os
from time import time
from gensim.corpora import Dictionary
import dblp
import itertools

output = sys.argv[1]

try:
    os.makedirs(os.path.dirname(output))
except:
    pass

docs = dblp.iter_document(dblp.iter_text())
docs = map(dblp.NGramProcessor(1), docs)

dct = Dictionary(docs)
dct.filter_extremes(no_below=10, no_above=0.2)
dct.save(output)
