from gensim.scripts.glove2word2vec import glove2word2vec
import os, sys

def die(*E, **F):
    print(*E, **F)
    sys.exit(0)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--glove', required=True)

    options = parser.parse_args()

    glove_path = options.glove
    if not os.path.exists(glove_path):
        die("cannot locate file")

    prefix, _ = os.path.splitext(glove_path)
    wvec_path = os.path.join(prefix + ".wvec")

    glove2word2vec(glove_path, wvec_path)
