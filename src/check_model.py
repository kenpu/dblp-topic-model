import sys
from gensim.corpora import Dictionary
from gensim.models.ldamodel import LdaModel
import argparse
import numpy as np
import textwrap

parser = argparse.ArgumentParser()
parser.add_argument('--dct', required=True)
parser.add_argument('--model', required=True)

args = parser.parse_args()

dct = Dictionary.load(args.dct)

model = LdaModel.load(args.model)

M = model.get_topics()

num_topics, num_terms = M.shape

# compute the TFIDF
DF = -np.log(np.sum(M, axis=0).reshape(1, num_terms) / num_topics)
N = M / DF

# Print the top words in each topic:
def print_topics(M, n):
    # rank by topic probability
    R = np.argsort(-M, axis=1)

    # print the top n words
    for t, I in enumerate(R[:,:n]):
        line = " ".join(dct[i] for i in I)
        line = textwrap.indent("\n".join(textwrap.wrap(line, 40)), "    ")
        yield ("[%d]:\n%s" % (t, line))

for x,y in zip(print_topics(M, 50), print_topics(N, 30)):
    print(x)
    print()
    #print(y)

