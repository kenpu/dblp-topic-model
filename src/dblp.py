import sys
import csv
import io
from nltk.tokenize import RegexpTokenizer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
import numpy as np

def YearChecker(year):
    if year == None:
        def checker(y):
            return True
    elif isinstance(year, list):
        y0, y1 = [str(x) for x in year]
        def checker(y):
            return y0 <= y <= y1
    else:
        y0 = str(year)
        def checker(y):
            return y0 == y
    return checker

_checkers = {
        '==': lambda value, pattern: pattern == value,
        '<:': lambda value, pattern: value in pattern,
        '>:': lambda value, pattner: pattern in value,
        '<>': lambda value, pattern1, pattern2: pattern1 <= value <= pattern2,
        '~~': lambda value, pattern: re.match(pattern, value)
        }
def Checker(query, columns):
    if query is None:
        def checker(row):
            return True
    else:
        query_cond = list(query.items())
        query_keys = [x[0] for x in query_cond]
        query_vals = [x[1] for x in query_cond]
        query_idx = [columns.index(k) for k in query_keys]
        cond = list(zip(query_idx, query_vals))
        checkers = []
        for i, v in cond:
            args = v.split()
            checkers.append((i, _checkers[args[0]], args[1:]))

        def checker(row):
            for i, pred, args in checkers:
                if not pred(row[i], *args):
                    return False
            return True
    return checker

def iter_text(tsv_file=None, column='title', 
        query=None, timestamp=False):
    if tsv_file:
        f = io.open(tsv_file, 'r', encoding='utf-8')
    else:
        f = sys.stdin

    rdr = csv.reader(f, delimiter='\t')
    columns = next(rdr)
    index = columns.index(column)
    year_index = columns.index('year')
    check = Checker(query, columns)

    for row in rdr:
        t = row[year_index]
        x = row[index]
        if check(row):
            yield (t, x) if timestamp else x

    if tsv_file:
        f.close()

STOPWORDS = set(stopwords.words('english'))

def is_good(token):
    if token.isnumeric():
        return False
    if len(token) <= 2:
        return False
    if token.lower() in STOPWORDS:
        return False

    return True

def NGramProcessor(n, include_doc=False, sep="_"):
    def f(doc):
        if n == 1:
            return doc

        grams = [sep.join(doc[i:i+n]) for i in range(len(doc) - n)]
        if include_doc:
            return doc + grams
        else:
            return grams
    return f

def iter_document(source, tokenizer=None, lemmatizer=None, **K):
    if isinstance(source, str):
        texts = iter_text(source, **K)
    else:
        texts = source

    if not tokenizer:
        tokenizer = RegexpTokenizer(r'\w+')
    if lemmatizer == True:
        lemmatizer = WordNetLemmatizer()

    for entry in texts:
        if isinstance(entry, tuple):
            t, text = entry
        else:
            t, text = None, entry
        text = text.lower()
        doc = [x for x in tokenizer.tokenize(text) if is_good(x)]
        if lemmatizer:
            doc = [lemmatizer.lemmatize(x) for x in doc]

        yield (t, doc) if t else doc

def iter_corpus(source, dct, **K):
    if isinstance(source, str):
        docs = iter_document(source, **K)
    else:
        docs = source

    for doc in docs:
        bow = dct.doc2bow(doc)
        if len(bow) > 0:
            yield bow


def docs2topicvec(docs, dct, model):
    n = model.num_topics
    X = np.zeros((len(docs), n))
    for i,d in enumerate(docs):
        if isinstance(d, tuple):
            bow = dct.doc2bow(d[-1])
        else:
            bow = dct.doc2bow(d)
        topicvec = model.get_document_topics(bow)
        for t, x in topicvec:
            X[i,t] = x
    return X

if __name__ == '__main__':
    from time import time

    texts = iter_text()
    ndocs = 0
    ntokens = 0
    s = time()
    for doc in map(NGramProcessor(1, True), iter_document(texts)):
        ndocs += 1
        ntokens += len(doc)
    print("Done in %.2f seconds" % (time() - s))
    print(ndocs, ntokens)
