import os
import dblp
from gensim.corpora import Dictionary
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.ldamodel import LdaModel
from gensim.models import KeyedVectors
import numpy as np
import matplotlib.pyplot as pl
from time import time

DATA = "../data"

def tsv(name):
    return os.path.join(DATA, 'tsv/%s.csv' % name)

def load_dct(name):
    dct_file = os.path.join(DATA, "dct/%s.dct" % name)
    return Dictionary.load(dct_file)

def build_model(name, dct, num_topics, **K):
    start = time()
    print("building name...", name)
    docs = dblp.iter_document(tsv(name), **K)
    corpus = list(dblp.iter_corpus(docs, dct))
    model = LdaMulticore(corpus=corpus, 
            num_topics=num_topics, 
            id2word=dct,
            workers=10)
    duration = time() - start
    print("  done in %.2f seconds" % duration)
    return model

def ldamodel(name, dct, num_topics=50, force=False, model_name=None, **K):
    if model_name:
        model_path = os.path.join(DATA, "models", model_name)
        if os.path.exists(model_path) and not force:
            model = LdaModel.load(model_path)
        else:
            model = build_model(name, dct, num_topics, **K)
        model.save(model_path)
    else:
        model = build_model(name, dct, num_topics, **K)

    return model

def topic_words(model, dct, 
        num_words=10, 
        topic=None, 
        keep_topic=True,
        keep_weights=False):
    if isinstance(model, np.ndarray):
        M = model
    else:
        M = model.get_topics()
    R = np.argsort(-M, axis=1)
    if topic is not None:
        I_topics = [topic]
    else:
        I_topics = list(range(R.shape[0]))

    
    results = []
    for topic, I_words in zip(I_topics, R[I_topics, :num_words]):
        words = [dct[i] for i in I_words]
        weights = M[topic, I_words]
        row = []
        if keep_topic:
            row.append(topic)

        row.append(words)

        if keep_weights:
            row.append(weights)

        results.append(row)
    return results

def transfer_matrix(M_from, M_to):
    (U, S, Vh) = np.linalg.svd(M_from, full_matrices=False)
    A = M_to @ Vh.T @ np.diag(1/S) @ U.T
    return A

def imshow(*E, **K):
    pl.imshow(*E, **K)
    pl.gca().get_xaxis().set_visible(False)
    pl.gca().get_yaxis().set_visible(False)
    

_wordvectors = None

def load_wv_model(Force=False):
    global _wordvectors

    start = time()
    if _wordvectors is None:
        print("Loading from disk - will take about 20 seconds...")
        model_path = os.path.join(DATA, 'glove/glove.6B.100d.wvec')
        _wordvectors = KeyedVectors.load_word2vec_format(
                model_path, binary=False)
    else:
        print("Loading from cache...")
    print("Wordvector model loaded in %.2f seconds" % (time() - start))
    return _wordvectors

def wv_get(wvmodel, x, dim=100):
    if x in wvmodel:
        return wvmodel[x]
    else:
        return np.zeros(dim)

def wv_encode(wvmodel, x):
    if isinstance(x, str):
        vecs = wv_get(wvmodel, x)
    elif isinstance(x[0], str):
        vecs = [wv_get(wvmodel,w) for w in x]
    elif isinstance(x[0][0], str):
        vecs = [[wv_get(wvmodel,w) for w in words] for words in x]
    else:
        raise Exception("Unknown layout for words container")

    return np.array(vecs)
